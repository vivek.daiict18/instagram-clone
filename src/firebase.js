import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyAf0r_q0RswOEeIBmHqCCceOOeCwyk9FVE",
  authDomain: "instagram-clone-244c4.firebaseapp.com",
  databaseURL: "https://instagram-clone-244c4.firebaseio.com",
  projectId: "instagram-clone-244c4",
  storageBucket: "instagram-clone-244c4.appspot.com",
  messagingSenderId: "298890788342",
  appId: "1:298890788342:web:544b276931c0bee8c11bd7",
});

const db = firebaseApp.firestore()
const auth = firebaseApp.auth()
const storage = firebaseApp.storage();

export {db, auth, storage};
