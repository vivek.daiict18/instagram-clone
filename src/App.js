import React, { useState, useEffect } from "react";
import "./App.css";
import Post from "./Post";
import { db, auth } from "./firebase";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
  Button,
} from "@material-ui/core";

function App() {
  const [posts, setPosts] = useState([]);
  const [open, setOpen] = useState(false);
  const [openSignIn, setOpenSignIn] = useState(false);
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [user, setUser] = useState(null);

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((authUser) => {
      if (authUser) {
        console.log(authUser);
        setUser(authUser);

        if (auth.displayName) {
        } else {
          return authUser.updateProfile({
            displayName: username,
          });
        }
      } else {
        setUser(null);
      }
    });

    return () => {
      unsubscribe();
    };
  }, [user, username]);

  useEffect(() => {
    db.collection("posts").onSnapshot((snapshot) => {
      setPosts(snapshot.docs.map((doc) => ({ id: doc.id, post: doc.data() })));
    });
  }, []);

  const signUp = (e) => {
    e.preventDefault();

    auth
      .createUserWithEmailAndPassword(email, password)
      .then((authUser) => {
        return authUser.user.updateProfile({
          displayName: username
        })
      })
      .catch((error) => alert(error.message));

      setOpen(false)
  };

  const signIn = (e) => {
    e.preventDefault()

    auth.signInWithEmailAndPassword(email, password)
    .catch(error => alert(error.message))

    setOpenSignIn(false)
  }

  return (
    <div className="app">
      <Dialog open={open} onClose={() => setOpen(false)}>
        <form>
          <DialogTitle id="form-dialog-title">
            <center>
              <img
                src="https://logos-world.net/wp-content/uploads/2020/04/Instagram-Logo.png"
                alt="Instagram Logo Sign Up"
                height="50"
              />
            </center>
          </DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              label="Username"
              type="text"
              onChange={(e) => setUsername(e.target.value)}
              fullWidth
            />
            <TextField
              margin="dense"
              label="Email Address"
              type="email"
              onChange={(e) => setEmail(e.target.value)}
              fullWidth
            />
            <TextField
              margin="dense"
              label="Password"
              type="password"
              onChange={(e) => setPassword(e.target.value)}
              fullWidth
            />
          </DialogContent>
          <DialogActions className="app__dialogActions">
            <Button type="submit" onClick={signUp} color="primary">
              Sign Up
            </Button>
          </DialogActions>
        </form>
      </Dialog>

      <Dialog open={openSignIn} onClose={() => setOpenSignIn(false)}>
        <form>
          <DialogTitle>
            <center>
              <img
                src="https://logos-world.net/wp-content/uploads/2020/04/Instagram-Logo.png"
                alt="Instagram Logo Sign Up"
                height="50"
              />
            </center>
          </DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              label="Email Address"
              type="email"
              onChange={(e) => setEmail(e.target.value)}
              fullWidth
            />
            <TextField
              margin="dense"
              label="Password"
              type="password"
              onChange={(e) => setPassword(e.target.value)}
              fullWidth
            />
          </DialogContent>
          <DialogActions className="app__dialogActions">
            <Button type="submit" onClick={signIn} color="primary">
              Login
            </Button>
          </DialogActions>
        </form>
      </Dialog>

      <div className="app__header">
        <img
          className="app__headerImage"
          src="https://logos-world.net/wp-content/uploads/2020/04/Instagram-Logo.png"
          alt="Instagram Logo"
          height="50"
        />
        {user ? (
          <Button
            size="small"
            className="app__login app__signOutButton"
            variant="contained"
            onClick={() => auth.signOut()}
          >
            Logout
          </Button>
        ) : (
          <div className="app__loginContainer">
            <Button
              size="small"
              className="app__login app__loginButton"
              variant="contained"
              onClick={() => setOpenSignIn(true)}
            >
              Login
            </Button>
            <Button
              size="small"
              className="app__login app__signUpButton"
              variant="contained"
              onClick={() => setOpen(true)}
            >
              Sign Up
            </Button>
          </div>
        )}
      </div>

      {posts.map(({ id, post }) => (
        <Post
          key={id}
          imageUrl={post.imageUrl}
          username={post.username}
          caption={post.caption}
        />
      ))}
    </div>
  );
}

export default App;
